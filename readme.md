# Personal userstyles for Stylus

Available Styles:

* Dark-UP :: /r/unixporn
* Dark-Learner :: uWaterloo LEARN

---

### Usage

1. Download Stylus on your browser (Chromium- or Firefox-based)
2. Go to **Manage Styles**
3. **Write new style**
4. Put in a fancy name for your style, paste the contents of the `.css` file, and save
5. Enjoy!
